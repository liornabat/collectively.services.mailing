﻿using Collectively.Common.Host;
using Collectively.Services.Mailing.Framework;
using Collectively.Messages.Commands.Mailing;

namespace Collectively.Services.Mailing
{
    public class Program
    {
        public static void Main(string[] args)
        {
            WebServiceHost
                .Create<Startup>(port: 10005)
                .UseAutofac(Bootstrapper.LifetimeScope)
                .UseRabbitMq(queueName: typeof(Program).Namespace)
                .SubscribeToCommand<SendResetPasswordEmailMessage>()
                .Build()
                .Run();
        }
    }
}
